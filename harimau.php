<?php

include_once "hewan.php";
include_once "fight.php";

class Harimau
{
    use Hewan, Fight;

    protected $jenis_hewan = 'Harimau';

    public function __construct()
    {
        $this->nama = 'Mufasa';
        $this->keahlian = 'lari cepat';
        $this->attackPower = 7;
        $this->defencePower = 8;
    }

    public function getInfoHewan()
    {
        return 'Jenis Hewan ' . $this->jenis_hewan . ', Bernama ' . $this->nama . ' memiliki jumlah kaki bernilai ' . $this->jumlahKaki = 4 .
            ' dan keahlian bernilai "' . $this->keahlian = 'lari cepat' .
            '", attackPower = ' . $this->attackPower .
            ', deffencePower = ' . $this->defencePower;
    }
}

$harimau = new Harimau();
